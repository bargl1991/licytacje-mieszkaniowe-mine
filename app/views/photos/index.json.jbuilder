json.array!(@photos) do |photo|
  json.extract! photo, :id, :event_id, :photo
  json.url photo_url(photo, format: :json)
end
