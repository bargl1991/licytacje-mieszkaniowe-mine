class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.text :description
      t.string :picture
      t.date :start_date
      t.time :start_time
      t.time :end_time
      t.string :location

      t.timestamps
    end
  end
end
